//A program to demonstrate serialization for various different dependencies
import java.io.*; 
class frst implements Serializable{
String name;
frst(String name){
	this.name=name;
}
}
public class scnd implements Serializable{
	int id;
	scnd(int id)
	{
		this.id=id;
		
	}
}
class couple extends frst 
{  
 public String location; 
 scnd S;   
 // constructor 
 public couple(String place,int id,String location) 
 { 
    super(place);
     S = new scnd( id );
     this.location = location;  
 } 
} 

class Serial 
{ 
 public static void main(String[] args) 
 {    
     couple object = new couple("sms", 3 , "hyderabad"); 
     String filename = "D://mine.txt"; 
       
     // Serialization  
     try
     {    
         //Saving of object in a file 
         FileOutputStream file = new FileOutputStream(filename); 
         ObjectOutputStream out = new ObjectOutputStream(file); 
           
         // Method for serialization of object 
         out.writeObject(object); 
           
         out.close(); 
         file.close(); 
           
         System.out.println("Object has been serialized"); 

     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
    }
       
 } 
} 